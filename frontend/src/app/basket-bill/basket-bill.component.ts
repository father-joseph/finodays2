import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-basket-bill',
  templateUrl: './basket-bill.component.html',
  styleUrls: ['./basket-bill.component.css']
})
export class BasketBillComponent implements OnInit {
  @Input() basket:any;
  quantity:any;
  summ:any;
  discount:any;
  cashback:any;

  constructor() { }

  ngOnInit(): void {
    this.quantity = this.basket.positions.map((i: { quantity: any; }) => i.quantity).reduce((partialSum:any, a:any) => partialSum + a, 0);
    this.summ = this.basket.total
    this.discount = - this.basket.payMethods[0].discount;
    this.cashback = this.basket.payMethods[0].cashback;
  }

  calculateTotal() {
    return this.summ + this.discount;
  }
}
