import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.css']
})
export class CheckComponent implements OnInit {
  @Input() total: any
  @Input() best:any
  constructor() { }

  ngOnInit(): void {
  }

}
