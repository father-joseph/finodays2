import { Component, OnInit } from '@angular/core';
import {RestService} from "../rest.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {
  basket:any;
  id:any;
  positions:any;
  payments:any
  bestPayment:any
  selectedPayment=0;
  pay:any;

  constructor(private restService: RestService,
              private route: ActivatedRoute,
              private router : Router) {

  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    this.restService.getBasket(this.id).subscribe(basket => {
      this.basket=basket
      this.bestPayment=this.basket.payMethods[0];
      this.payments = this.basket.payMethods.slice(1);
      this.pay=this.bestPayment.total;
    });
  }

  gotoBasket() {
    this.router.navigateByUrl('/basket', { state: this.basket });
  }

  requestPay() {
    this.restService.requestPayment(this.id);
    this.router.navigateByUrl('/success', { state: this.basket.positions });
  }

}
