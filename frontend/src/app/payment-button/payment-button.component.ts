import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-payment-button',
  templateUrl: './payment-button.component.html',
  styleUrls: ['./payment-button.component.css']
})
export class PaymentButtonComponent implements OnInit {
  @Input() amount: any;

  constructor() { }

  ngOnInit(): void {
  }

}
