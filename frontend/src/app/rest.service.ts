import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  private readonly cronName = 'recalculateCategoryJob';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json','clientId':'1'})
  };

  constructor(private http: HttpClient) {
  }

  requestPayment(basketId: any) {
    return this.http.post("/api/v1/checkout/pay", {basketId}, this.httpOptions);
  }

  getBasket(busketId: any): Observable<any> {
    return this.http.get<any>("/api/v1/checkout/" + busketId, this.httpOptions);
  }
}
