import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-basket-list',
  templateUrl: './basket-list.component.html',
  styleUrls: ['./basket-list.component.css']
})
export class BasketListComponent implements OnInit {
  @Input() basketlist: any;

  constructor() {
  }

  ngOnInit(): void {
  }

  public calculateTotal(item: { quantity: number; price: number; }) {
    return item.quantity * item.price;
  }

}
