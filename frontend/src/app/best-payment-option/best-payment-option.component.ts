import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-best-payment-option',
  templateUrl: './best-payment-option.component.html',
  styleUrls: ['./best-payment-option.component.css']
})
export class BestPaymentOptionComponent implements OnInit {
  @Input() payment: any;

  constructor() { }

  ngOnInit(): void {
    console.log(this.payment)
  }

}
