import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BestPaymentOptionComponent } from './best-payment-option.component';

describe('BestPaymentOptionComponent', () => {
  let component: BestPaymentOptionComponent;
  let fixture: ComponentFixture<BestPaymentOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BestPaymentOptionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BestPaymentOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
