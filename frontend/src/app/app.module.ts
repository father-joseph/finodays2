import {NgDompurifySanitizer} from "@tinkoff/ng-dompurify";
import {HttpClientModule} from '@angular/common/http';
import {
  TuiRootModule,
  TuiDialogModule,
  TuiAlertModule,
  TUI_SANITIZER,
  TuiSvgModule,
  TuiButtonModule, TuiExpandModule
} from "@taiga-ui/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {QRCodeModule} from 'angularx-qrcode';
import { PaymentComponent } from './payment/payment.component';
import {TuiAccordionModule, TuiBadgeModule} from "@taiga-ui/kit";
import { BasketComponent } from './basket/basket.component';
import { BasketListComponent } from './basket-list/basket-list.component';
import { ShopComponent } from './shop/shop.component';
import { CheckComponent } from './check/check.component';
import { BestPaymentOptionComponent } from './best-payment-option/best-payment-option.component';
import { PanelComponent } from './panel/panel.component';
import { BasketButtonComponent } from './basket-button/basket-button.component';
import { PaymentButtonComponent } from './payment-button/payment-button.component';
import { SuccessComponent } from './success/success.component';
import {TuiFormatNumberPipeModule} from '@taiga-ui/core';
import { BasketBillComponent } from './basket-bill/basket-bill.component';

@NgModule({
  declarations: [
    AppComponent,
    PaymentComponent,
    BasketComponent,
    BasketListComponent,
    ShopComponent,
    CheckComponent,
    BestPaymentOptionComponent,
    PanelComponent,
    BasketButtonComponent,
    PaymentButtonComponent,
    SuccessComponent,
    BasketBillComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    QRCodeModule,
    BrowserAnimationsModule,
    TuiRootModule,
    TuiDialogModule,
    TuiAlertModule,
    TuiAccordionModule,
    TuiSvgModule,
    TuiButtonModule,
    TuiBadgeModule,
    TuiExpandModule,
    HttpClientModule,
    TuiFormatNumberPipeModule,
    QRCodeModule
  ],
  providers: [{provide: TUI_SANITIZER, useClass: NgDompurifySanitizer}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
