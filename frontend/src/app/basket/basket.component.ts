import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RestService} from "../rest.service";

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {
  basket:any
  basketlist:any


  constructor(private restService: RestService, private router:Router) {
    // @ts-ignore
    this.basket=this.router.getCurrentNavigation().extras.state
    console.log(this.basket)
    this.basketlist = this.basket.positions;
  }

  ngOnInit(): void {
  }

  requestPay() {
    this.restService.requestPayment(1);
    this.router.navigateByUrl('/success', { state: this.basket.positions });
  }

}
