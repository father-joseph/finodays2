import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PanelComponent} from "./panel/panel.component";
import {BasketComponent} from "./basket/basket.component";
import {SuccessComponent} from "./success/success.component";

const routes: Routes = [
  { path: 'success', component: SuccessComponent },
  { path: 'basket', component: BasketComponent },
  { path: 'buy/:id', component: PanelComponent },
  { path: '**', component: PanelComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
