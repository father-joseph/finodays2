import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  requestBasketId(basket:any) {

    return this.http.post<any>("/api/v1/joseph/create-basket", {"sellerId": 2, "positions":basket}, this.httpOptions);
  }
}
