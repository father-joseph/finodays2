import {Component, OnInit} from '@angular/core';
import {RestService} from "../rest.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {
  gen = false;
  url = ""
  form: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    quantity: new FormControl(1, Validators.required),
  });

  // Товары появляются в результате сканирования на кассе, заглушка.
  items = [
    {
      "name": "Автоматический выключатель тока 1,433мм  ",
      "price": 274,
      "quantity": 1,
    },
    {
      "name": "Зарядное устройство для аккумуляторов",
      "price": 433,
      "quantity": 1,
    },
    {
      "name": "Воздушно-цинковые батарейки",
      "price": 2554,
      "quantity": 2,
    }
  ]

  constructor(private restService: RestService) {
  }

  ngOnInit(): void {
  }

  public calculateTotal(item: { quantity: number; price: number; }) {
    return item.quantity * item.price;
  }

  genQr() {
    console.log(this.items)
    this.restService.requestBasketId(this.items)
      .subscribe(basket => {
          this.url = "ec2-3-80-60-166.compute-1.amazonaws.com/buy/" + basket.basketId;
          this.gen = true;
        }
      )
  }

  addProduct() {
    this.items.push(this.form.value);
  }

  remove(i: any) {
    this.items = this.items.filter(function (item) {
      return item != i
    })
  }
}
