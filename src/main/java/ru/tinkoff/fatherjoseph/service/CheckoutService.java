package ru.tinkoff.fatherjoseph.service;

import lombok.NonNull;
import ru.tinkoff.fatherjoseph.dto.checkout.CheckoutResultDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentRequestDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentResultDto;

public interface CheckoutService {
    CheckoutResultDto checkout(@NonNull Long clientId, @NonNull Long basketId);

    PaymentResultDto pay(PaymentRequestDto paymentRequestDto);
}
