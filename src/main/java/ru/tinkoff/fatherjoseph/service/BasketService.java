package ru.tinkoff.fatherjoseph.service;

import ru.tinkoff.fatherjoseph.dto.basket.CreateBasketDto;
import ru.tinkoff.fatherjoseph.dto.basket.CreateUpdateBasketResultDto;
import ru.tinkoff.fatherjoseph.dto.basket.DeleteBasketResultDto;
import ru.tinkoff.fatherjoseph.dto.basket.UpdateBasketDto;
import ru.tinkoff.fatherjoseph.model.Basket;

public interface BasketService {
    CreateUpdateBasketResultDto create(CreateBasketDto createBasketDto);

    CreateUpdateBasketResultDto updateBasket(UpdateBasketDto updateBasketDto);

    DeleteBasketResultDto deleteBasket(Long basketId);

    Basket getBasketById(Long basketId);
}
