package ru.tinkoff.fatherjoseph.service;

import ru.tinkoff.fatherjoseph.model.Client;

public interface ClientService {
    Client createClient(Client client);
}
