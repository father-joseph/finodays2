package ru.tinkoff.fatherjoseph.service;

import ru.tinkoff.fatherjoseph.model.Seller;

public interface SellerService {
    Seller createSeller(Seller seller);
}
