package ru.tinkoff.fatherjoseph.service;

import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowRequestDto;

public interface BankService {
    PaymentMethodShowDto getTotalWithBonuses(PaymentMethodShowRequestDto request);

    String getBankId();
}
