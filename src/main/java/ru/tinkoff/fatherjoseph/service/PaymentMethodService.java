package ru.tinkoff.fatherjoseph.service;

import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodDto;

import java.util.List;

public interface PaymentMethodService {

    List<PaymentMethodDto> getClientPaymentMethods(Long clientId);
}
