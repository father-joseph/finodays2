package ru.tinkoff.fatherjoseph.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.fatherjoseph.dto.basket.CreateBasketDto;
import ru.tinkoff.fatherjoseph.dto.basket.CreateUpdateBasketResultDto;
import ru.tinkoff.fatherjoseph.dto.basket.DeleteBasketResult;
import ru.tinkoff.fatherjoseph.dto.basket.DeleteBasketResultDto;
import ru.tinkoff.fatherjoseph.dto.position.PositionDto;
import ru.tinkoff.fatherjoseph.dto.basket.UpdateBasketDto;
import ru.tinkoff.fatherjoseph.exception.BusinessException;
import ru.tinkoff.fatherjoseph.exception.NotFoundException;
import ru.tinkoff.fatherjoseph.model.Basket;
import ru.tinkoff.fatherjoseph.model.Position;
import ru.tinkoff.fatherjoseph.model.enums.BasketStatus;
import ru.tinkoff.fatherjoseph.repository.BasketRepository;
import ru.tinkoff.fatherjoseph.repository.PositionRepository;
import ru.tinkoff.fatherjoseph.service.BasketService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {

    private final BasketRepository basketRepository;

    private final PositionRepository positionRepository;

    private final Set<BasketStatus> rejectForbiddenStatuses = Set.of(BasketStatus.DONE, BasketStatus.IN_TRANSACTION);

    @Override
    @Transactional
    public CreateUpdateBasketResultDto create(CreateBasketDto createBasketDto) {
        final Basket basket = new Basket();
        basket.setSellerId(createBasketDto.getSellerId());

        final Basket savedBasket = basketRepository.save(basket);
        final Long basketId = savedBasket.getBasketId();
        saveNewPositions(createBasketDto.getPositions(), basketId);

        return new CreateUpdateBasketResultDto(basketId);
    }

    @Override
    @Transactional
    public CreateUpdateBasketResultDto updateBasket(UpdateBasketDto updateBasketDto) {
        Long basketId = updateBasketDto.getBasketId();
        Basket basket = getBasketById(basketId);
        if (!Objects.equals(basket.getStatus(), BasketStatus.WAITING)) {
            throw new BusinessException("Basket in status %s cannot be modified".formatted(basket.getStatus()));
        }
        positionRepository.deleteAllByBasketId(basketId);
        saveNewPositions(updateBasketDto.getPositions(), basketId);

        return new CreateUpdateBasketResultDto(basketId);
    }

    @Override
    @Transactional
    public DeleteBasketResultDto deleteBasket(Long basketId) {
        Basket basket = getBasketById(basketId);
        if (rejectForbiddenStatuses.contains(basket.getStatus())) {
            throw new BusinessException("Basket in status %s cannot be rejected".formatted(basket.getStatus()));
        }
        return new DeleteBasketResultDto(DeleteBasketResult.OK);
    }

    @Override
    @Transactional
    public Basket getBasketById(Long basketId) {
        return basketRepository.findById(basketId)
            .orElseThrow(() -> new NotFoundException("Basket with id %s not found".formatted(basketId)));
    }

    private void saveNewPositions(List<PositionDto> positionDtos, Long basketId) {
        List<Position> positions = positionDtos
            .stream()
            .map(it -> toPosition(it, basketId))
            .toList();
        positionRepository.saveAll(positions);
    }

    private Position toPosition(final PositionDto positionDto, final Long basketId) {
        final Position position = new Position();
        position.setBasketId(basketId);
        position.setName(positionDto.getName());
        position.setPrice(positionDto.getPrice());
        position.setQuantity(positionDto.getQuantity());
        return position;
    }
}
