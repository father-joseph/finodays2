package ru.tinkoff.fatherjoseph.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fatherjoseph.dto.checkout.CheckoutResultDto;
import ru.tinkoff.fatherjoseph.dto.position.PositionDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowRequestDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentRequestDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentResultDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentResultType;
import ru.tinkoff.fatherjoseph.dto.seller.SellerInfoDto;
import ru.tinkoff.fatherjoseph.exception.InternalServerErrorException;
import ru.tinkoff.fatherjoseph.external.api.NspkClient;
import ru.tinkoff.fatherjoseph.mappers.PositionMapper;
import ru.tinkoff.fatherjoseph.mappers.SellerMapper;
import ru.tinkoff.fatherjoseph.model.Basket;
import ru.tinkoff.fatherjoseph.model.Seller;
import ru.tinkoff.fatherjoseph.repository.PositionRepository;
import ru.tinkoff.fatherjoseph.repository.SellerRepository;
import ru.tinkoff.fatherjoseph.service.BankService;
import ru.tinkoff.fatherjoseph.service.BasketService;
import ru.tinkoff.fatherjoseph.service.CheckoutService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CheckoutServiceImpl implements CheckoutService {

    private final BasketService basketService;
    private final PositionRepository positionRepository;
    private final SellerRepository sellerRepository;

    private final PositionMapper positionMapper;

    private final SellerMapper sellerMapper;

    private final NspkClient nspkClient;

    private final List<BankService> bankServices;

    private final ExecutorService payMethodBonusCollectorExecutor;

    @Override
    @Transactional
    public CheckoutResultDto checkout(@NonNull Long clientId, @NonNull Long basketId) {
        CompletableFuture<List<PaymentMethodDto>> paymentMethodsFuture =
            CompletableFuture.supplyAsync(
                () -> nspkClient.getClientPaymentMethods(clientId),
                payMethodBonusCollectorExecutor
            );

        final Basket basket = basketService.getBasketById(basketId);
        final Seller seller = sellerRepository.getSellerByBasketId(basketId);
        final SellerInfoDto sellerInfoDto = sellerMapper.mapToSellerInfoDto(seller);
        // should redirect to 'successfully paid' if basket status is DONE,
        // do not allow to pay again if status not Waiting
        final Integer basketVersion = basket.getVersion();
        final List<PositionDto> positions = positionRepository.findAllByBasketId(basketId)
            .stream()
            .map(positionMapper::mapToPositionDto)
            .toList();

        try {
            Set<String> clientBankIds = paymentMethodsFuture.get()
                .stream()
                .map(PaymentMethodDto::getBankId)
                .collect(Collectors.toSet());

            PaymentMethodShowRequestDto totalWithBonusesRequestDto = new PaymentMethodShowRequestDto();
            totalWithBonusesRequestDto.setPositions(positions);
            totalWithBonusesRequestDto.setClientId(clientId);

            List<CompletableFuture<PaymentMethodShowDto>> paymentMethodFuturesList =
                bankServices.stream()
                    .filter(it -> clientBankIds.contains(it.getBankId()))
                    .map(bankService ->
                        CompletableFuture.supplyAsync(() -> bankService.getTotalWithBonuses(totalWithBonusesRequestDto)))
                    .toList();

            List<PaymentMethodShowDto> paymentMethodShowDtos = new ArrayList<>();
            for (var future : paymentMethodFuturesList) {
                PaymentMethodShowDto paymentMethodShowDto = future.get();
                paymentMethodShowDtos.add(paymentMethodShowDto);
            }

            return new CheckoutResultDto(
                sellerInfoDto,
                basketVersion,
                positions,
                sortByMostBeneficial(paymentMethodShowDtos),
                countTotal(positions)
            );
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    private List<PaymentMethodShowDto> sortByMostBeneficial(List<PaymentMethodShowDto> paymentMethods) {
        return paymentMethods.stream()
            .sorted(Comparator.comparingInt(this::getClientCost))
            .toList();
    }

    private Integer countTotal(List<PositionDto> positions) {
        return positions.stream()
            .map(it -> it.getQuantity() * it.getPrice())
            .reduce(Integer::sum)
            .orElseThrow(() -> new InternalServerErrorException("No payment methods found"));
    }

    private int getClientCost(PaymentMethodShowDto paymentMethod) {
        return paymentMethod.getTotal() - paymentMethod.getCashback();
    }

    @Override
    @Transactional
    public PaymentResultDto pay(PaymentRequestDto paymentRequestDto) {
        // compare basket version, redirect to checkout page if does not match
        // send request to payment system with secretly stored token, retrieved from PaymentMethodDetailsDto
        return new PaymentResultDto(PaymentResultType.OK);
    }
}
