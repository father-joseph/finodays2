package ru.tinkoff.fatherjoseph.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fatherjoseph.model.Client;
import ru.tinkoff.fatherjoseph.repository.ClientRepository;
import ru.tinkoff.fatherjoseph.service.ClientService;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;

    @Override
    @Transactional
    public Client createClient(Client Client) {
        return clientRepository.save(Client);
    }
}
