package ru.tinkoff.fatherjoseph.service.impl;

import org.springframework.stereotype.Service;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodDto;
import ru.tinkoff.fatherjoseph.service.PaymentMethodService;

import java.util.List;

/**
 * NSPK or other service, which is supposed to be able to return client
 * payment methods with available balance.
 */
@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {

    @Override
    public List<PaymentMethodDto> getClientPaymentMethods(Long clientId) {
        PaymentMethodDto p1 = new PaymentMethodDto();
        PaymentMethodDto p2 = new PaymentMethodDto();
        PaymentMethodDto p3 = new PaymentMethodDto();

        p1.setBankId("tinkoffId");
        p2.setBankId("sberPayId");
        p3.setBankId("digitRubId");
        return List.of(p1, p2, p3);
    }
}
