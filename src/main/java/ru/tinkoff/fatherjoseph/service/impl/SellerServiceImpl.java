package ru.tinkoff.fatherjoseph.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fatherjoseph.model.Seller;
import ru.tinkoff.fatherjoseph.repository.SellerRepository;
import ru.tinkoff.fatherjoseph.service.SellerService;

@Service
@RequiredArgsConstructor
public class SellerServiceImpl implements SellerService {
    private final SellerRepository sellerRepository;

    @Override
    @Transactional
    public Seller createSeller(Seller seller) {
        return sellerRepository.save(seller);
    }
}
