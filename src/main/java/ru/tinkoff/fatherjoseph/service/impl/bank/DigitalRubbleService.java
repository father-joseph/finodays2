package ru.tinkoff.fatherjoseph.service.impl.bank;

import org.springframework.stereotype.Service;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowRequestDto;
import ru.tinkoff.fatherjoseph.service.BankService;
import ru.tinkoff.fatherjoseph.utils.MockUtils;

@Service
public class DigitalRubbleService implements BankService {
    @Override
    public PaymentMethodShowDto getTotalWithBonuses(PaymentMethodShowRequestDto request) {
        return MockUtils.mockBankService(request, "Sber", "mir", 19541, 0.01, 0.015);
    }

    @Override
    public String getBankId() {
        return "digitRubId";
    }
}
