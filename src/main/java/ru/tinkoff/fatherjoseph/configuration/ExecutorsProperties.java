package ru.tinkoff.fatherjoseph.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import ru.tinkoff.fatherjoseph.configuration.properties.ExecutorProperties;

@ConstructorBinding
@ConfigurationProperties(prefix = "executor")
public record ExecutorsProperties(ExecutorProperties bonusCollector) {
}
