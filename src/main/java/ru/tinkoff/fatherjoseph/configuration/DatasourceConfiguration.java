package ru.tinkoff.fatherjoseph.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class DatasourceConfiguration {

    @Bean
    DataSource dataSource(HikariConfig config) {
        return new HikariDataSource(config);
    }

    @Configuration
    @ConfigurationProperties("datasources.joseph-postgres")
    static class JosephPostgresConfig extends HikariConfig {
    }
}
