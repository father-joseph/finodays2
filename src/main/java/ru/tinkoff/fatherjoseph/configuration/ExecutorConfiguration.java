package ru.tinkoff.fatherjoseph.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tinkoff.fatherjoseph.configuration.properties.ExecutorProperties;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
@RequiredArgsConstructor
public class ExecutorConfiguration {

    private final ExecutorsProperties executorsProperties;

    @Bean
    public ExecutorService payMethodBonusCollectorExecutor() {
        ExecutorProperties executorProperties = executorsProperties.bonusCollector();
        return new ThreadPoolExecutor(
            executorProperties.corePoolSize(),
            executorProperties.maxPoolSize(),
            executorProperties.keepAliveSec(), TimeUnit.SECONDS,
            new LinkedBlockingQueue<>()
        );
    }
}
