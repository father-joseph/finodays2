package ru.tinkoff.fatherjoseph.configuration.properties;

import org.springframework.boot.context.properties.ConstructorBinding;

@ConstructorBinding
public record ExecutorProperties(
    int corePoolSize,
    int maxPoolSize,
    int keepAliveSec) {
}
