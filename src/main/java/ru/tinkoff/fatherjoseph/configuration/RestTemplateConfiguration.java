package ru.tinkoff.fatherjoseph.configuration;


import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriTemplateHandler;

@Configuration
public class RestTemplateConfiguration {

    @Bean
    @Primary
    public RestTemplate restClient() {
        return new RestTemplateBuilder()
            .uriTemplateHandler(getEncodeUriBuilderFactory())
            .build();
    }

    private UriTemplateHandler getEncodeUriBuilderFactory() {
        DefaultUriBuilderFactory uriBuilderFactory = new DefaultUriBuilderFactory();
        uriBuilderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.TEMPLATE_AND_VALUES);
        return uriBuilderFactory;
    }
}
