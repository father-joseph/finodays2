package ru.tinkoff.fatherjoseph.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "positions")
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Position {
    private static final String BASKET_ID_COLUMN = "basket_id";

    @Id
    @SequenceGenerator(name = "position_seq", sequenceName = "position_sequence", allocationSize = 20)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_seq")
    private Long positionId;
    private Long basketId;

    private String name;
    private Integer price;
    private Integer quantity;
}
