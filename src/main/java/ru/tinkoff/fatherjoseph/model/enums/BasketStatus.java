package ru.tinkoff.fatherjoseph.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.tinkoff.fatherjoseph.exception.InternalServerErrorException;

@RequiredArgsConstructor
public enum BasketStatus implements Valuable {

    WAITING("Waiting"), IN_TRANSACTION("InTransaction"), DONE("Done"), REJECTED("Rejected");

    @Getter
    @JsonValue
    private final String value;

    @Override
    public String toString() {
        return value;
    }

    public static BasketStatus getByValue(String value) {
        return Valuable.findByValue(values(), value)
            .orElseThrow(() -> new InternalServerErrorException(
                String.format("Unsupported task result status = '%s'", value)
            ));
    }
}
