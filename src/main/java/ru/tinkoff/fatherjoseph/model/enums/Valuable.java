package ru.tinkoff.fatherjoseph.model.enums;

import java.util.Arrays;
import java.util.Optional;

public interface Valuable {

    String getValue();

    static <T extends Enum<T> & Valuable> Optional<T> findByValue(T[] values, String value) {
        return Arrays.stream(values)
                .filter(t -> t.getValue().equals(value))
                .findFirst();
    }
}
