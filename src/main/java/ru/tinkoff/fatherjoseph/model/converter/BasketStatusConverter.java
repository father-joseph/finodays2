package ru.tinkoff.fatherjoseph.model.converter;

import ru.tinkoff.fatherjoseph.model.enums.BasketStatus;

import javax.persistence.AttributeConverter;

public class BasketStatusConverter implements AttributeConverter<BasketStatus, String> {

    @Override
    public String convertToDatabaseColumn(BasketStatus status) {
        if (status == null) {
            return null;
        }
        return status.getValue();
    }

    @Override
    public BasketStatus convertToEntityAttribute(String dbStatus) {
        if (dbStatus == null) {
            return null;
        }
        return BasketStatus.getByValue(dbStatus);
    }
}
