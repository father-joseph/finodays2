package ru.tinkoff.fatherjoseph.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.tinkoff.fatherjoseph.model.converter.BasketStatusConverter;
import ru.tinkoff.fatherjoseph.model.enums.BasketStatus;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "baskets")
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Basket {
    // should be set up more precisely, use UUID for example
    @Id
    @SequenceGenerator(name = "basket_seq", sequenceName = "basket_sequence", allocationSize = 20)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "basket_seq")
    private Long basketId;

    private Long sellerId;

    @NonNull
    @Column(length = 30, nullable = false)
    @Convert(converter = BasketStatusConverter.class)
    private BasketStatus status = BasketStatus.WAITING;

    @Version
    private int version;

    //     todo: do i need it?
    //    @OneToMany
    //    private List<Position> positions;
}
