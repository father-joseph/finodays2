package ru.tinkoff.fatherjoseph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = {"ru.tinkoff.fatherjoseph.configuration"})
public class FatherJosephApplication {

    public static void main(String[] args) {
        SpringApplication.run(FatherJosephApplication.class, args);
    }
}
