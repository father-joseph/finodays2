package ru.tinkoff.fatherjoseph.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tinkoff.fatherjoseph.model.Position;

import java.util.List;

public interface PositionRepository extends JpaRepository<Position, Long> {

    List<Position> findAllByBasketId(Long basketId);

    void deleteAllByBasketId(Long basketId);
}
