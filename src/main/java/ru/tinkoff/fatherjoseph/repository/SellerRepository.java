package ru.tinkoff.fatherjoseph.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.tinkoff.fatherjoseph.model.Seller;

public interface SellerRepository extends JpaRepository<Seller, Long> {
    @Query("select s " +
           "from " +
           "    Seller s left join Basket b on s.sellerId = b.sellerId " +
           "where b.basketId = :basketId")
    Seller getSellerByBasketId(Long basketId);
}
