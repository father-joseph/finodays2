package ru.tinkoff.fatherjoseph.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tinkoff.fatherjoseph.model.Basket;

public interface BasketRepository extends JpaRepository<Basket, Long> {
}
