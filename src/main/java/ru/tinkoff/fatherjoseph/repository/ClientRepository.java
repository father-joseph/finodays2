package ru.tinkoff.fatherjoseph.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tinkoff.fatherjoseph.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
