package ru.tinkoff.fatherjoseph.external.api;

import org.springframework.stereotype.Service;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodDto;

import java.util.List;

@Service
public class NspkClient {
    /**
     * This should be available later
     */
    public List<PaymentMethodDto> getClientPaymentMethods(Long clientId) {
        PaymentMethodDto method1 = new PaymentMethodDto();
        PaymentMethodDto method2 = new PaymentMethodDto();
        PaymentMethodDto method3 = new PaymentMethodDto();
        method1.setBankId("tinkoffId");
        method2.setBankId("sberPayId");
        method3.setBankId("digitRubId");

        return List.of(method1, method2, method3);
    }
}
