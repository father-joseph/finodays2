package ru.tinkoff.fatherjoseph.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fatherjoseph.dto.basket.CreateBasketDto;
import ru.tinkoff.fatherjoseph.dto.basket.CreateUpdateBasketResultDto;
import ru.tinkoff.fatherjoseph.dto.basket.DeleteBasketResultDto;
import ru.tinkoff.fatherjoseph.dto.basket.UpdateBasketDto;
import ru.tinkoff.fatherjoseph.service.BasketService;

@RestController
@RequestMapping(BasketController.ROOT_MAPPING)
@RequiredArgsConstructor
@Tag(name = "joseph-pay", description = "API магазина для работы с Joseph Pay")
public class BasketController {

    public static final String ROOT_MAPPING = "/api/v1/joseph";

    public static final String CREATE_MAPPING = "/create-basket";
    private static final String UPDATE_MAPPING = "/update-basket";
    private static final String DELETE_MAPPING = "/delete-basket";

    private final BasketService basketService;

    @PostMapping(CREATE_MAPPING)
    @Operation(summary = "Basket creation")
    public CreateUpdateBasketResultDto createBasket(@RequestBody CreateBasketDto createBasketDto) {
        return basketService.create(createBasketDto);
    }

    @PostMapping(UPDATE_MAPPING)
    @Operation(summary = "Basket update")
    public CreateUpdateBasketResultDto updateBasket(@RequestBody UpdateBasketDto updateBasketDto) {
        return basketService.updateBasket(updateBasketDto);
    }

    @DeleteMapping(DELETE_MAPPING)
    @Operation(summary = "Basket deletion")
    public DeleteBasketResultDto deleteBasket(@RequestParam Long basketId) {
        return basketService.deleteBasket(basketId);
    }
}

