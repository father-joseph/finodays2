package ru.tinkoff.fatherjoseph.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fatherjoseph.dto.client.RegisterClientRequestDto;
import ru.tinkoff.fatherjoseph.dto.client.RegisterClientResultDto;
import ru.tinkoff.fatherjoseph.dto.seller.RegisterSellerRequestDto;
import ru.tinkoff.fatherjoseph.dto.seller.RegisterSellerResultDto;
import ru.tinkoff.fatherjoseph.mappers.ClientMapper;
import ru.tinkoff.fatherjoseph.mappers.SellerMapper;
import ru.tinkoff.fatherjoseph.model.Client;
import ru.tinkoff.fatherjoseph.model.Seller;
import ru.tinkoff.fatherjoseph.service.ClientService;
import ru.tinkoff.fatherjoseph.service.SellerService;

/**
 * Methods for users and sellers registration.
 * Seller registration methods should be hidden.
 */
@RestController
@RequestMapping(UserController.ROOT_MAPPING)
@RequiredArgsConstructor
@Tag(name = "joseph-pay-users", description = "API для регистрации пользователей")
public class UserController {

    public static final String ROOT_MAPPING = "/api/v1/register";

    private static final String REGISTER_SELLER_MAPPING = "/seller";
    private static final String REGISTER_CLIENT_MAPPING = "/client";

    private final SellerMapper sellerMapper;
    private final SellerService sellerService;

    private final ClientMapper clientMapper;
    private final ClientService clientService;

    @PostMapping(REGISTER_SELLER_MAPPING)
    @Operation(summary = "Register seller")
    public RegisterSellerResultDto registerSeller(@RequestBody RegisterSellerRequestDto registerSellerRequestDto) {
        Seller seller = sellerMapper.mapToSeller(registerSellerRequestDto);
        Seller savedSeller = sellerService.createSeller(seller);
        return sellerMapper.mapToRegisterSellerResultDto(savedSeller);
    }

    @PostMapping(REGISTER_CLIENT_MAPPING)
    @Operation(summary = "Register seller")
    public RegisterClientResultDto registerClient(@RequestBody RegisterClientRequestDto registerClientRequestDto) {
        Client client = clientMapper.mapToSeller(registerClientRequestDto);
        Client savedSeller = clientService.createClient(client);
        return clientMapper.mapToRegisterSellerResultDto(savedSeller);
    }
}
