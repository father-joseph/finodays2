package ru.tinkoff.fatherjoseph.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fatherjoseph.dto.checkout.CheckoutResultDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentRequestDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentResultDto;
import ru.tinkoff.fatherjoseph.service.CheckoutService;

/**
 * Methods for checkout and payment.
 * These methods should be allowed after authentication only.
 */
@RestController
@RequestMapping(CheckoutController.ROOT_MAPPING)
@RequiredArgsConstructor
@Tag(name = "joseph-pay-checkout", description = "Клиентское API для работы с корзиной")
public class CheckoutController {

    public static final String ROOT_MAPPING = "/api/v1/checkout";

    private static final String CHECKOUT_MAPPING = "/{basketId}";
    private static final String PAY_MAPPING = "/pay";

    private static final String CLIENT_ID = "clientId";

    private final CheckoutService checkoutService;

    @GetMapping(CHECKOUT_MAPPING)
    @Operation(summary = "Get checkout details")
    public CheckoutResultDto checkout(@RequestHeader(name = CLIENT_ID) Long clientId,
                                      @PathVariable Long basketId) {
        return checkoutService.checkout(clientId, basketId);
    }

    @PostMapping(PAY_MAPPING)
    @Operation(summary = "Process payment")
    public PaymentResultDto pay(@RequestBody PaymentRequestDto paymentRequestDto) {
        return checkoutService.pay(paymentRequestDto);
    }
}
