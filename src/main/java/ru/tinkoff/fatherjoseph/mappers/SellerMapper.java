package ru.tinkoff.fatherjoseph.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.tinkoff.fatherjoseph.dto.seller.RegisterSellerRequestDto;
import ru.tinkoff.fatherjoseph.dto.seller.RegisterSellerResultDto;
import ru.tinkoff.fatherjoseph.dto.seller.SellerInfoDto;
import ru.tinkoff.fatherjoseph.model.Seller;

@Mapper
public interface SellerMapper {
    SellerInfoDto mapToSellerInfoDto(Seller seller);

    RegisterSellerResultDto mapToRegisterSellerResultDto(Seller seller);

    @Mapping(target = "sellerId", ignore = true)
    Seller mapToSeller(RegisterSellerRequestDto registerSellerRequestDto);
}
