package ru.tinkoff.fatherjoseph.mappers;

import org.mapstruct.Mapper;
import ru.tinkoff.fatherjoseph.dto.position.PositionDto;
import ru.tinkoff.fatherjoseph.model.Position;

@Mapper
public interface PositionMapper {

    PositionDto mapToPositionDto(Position position);
}
