package ru.tinkoff.fatherjoseph.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.tinkoff.fatherjoseph.dto.client.RegisterClientRequestDto;
import ru.tinkoff.fatherjoseph.dto.client.RegisterClientResultDto;
import ru.tinkoff.fatherjoseph.model.Client;

@Mapper
public interface ClientMapper {
    RegisterClientResultDto mapToRegisterSellerResultDto(Client client);

    @Mapping(target = "clientId", ignore = true)
    Client mapToSeller(RegisterClientRequestDto registerClientRequestDto);
}
