package ru.tinkoff.fatherjoseph.dto.checkout;

import lombok.Data;
import lombok.NonNull;
import ru.tinkoff.fatherjoseph.dto.position.PositionDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowDto;
import ru.tinkoff.fatherjoseph.dto.seller.SellerInfoDto;

import java.util.List;

@Data
public class CheckoutResultDto {
    @NonNull
    private SellerInfoDto sellerInfo;
    @NonNull
    private Integer basketVersion;
    @NonNull
    private List<PositionDto> positions;
    @NonNull
    private List<PaymentMethodShowDto> payMethods;
    @NonNull
    private Integer total; // price for client
}
