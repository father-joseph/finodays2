package ru.tinkoff.fatherjoseph.dto.position;

import lombok.Data;
import lombok.NonNull;

@Data
public class PositionDto {
    @NonNull
    private String name;
    @NonNull
    private Integer price;
    @NonNull
    private Integer quantity;
}
