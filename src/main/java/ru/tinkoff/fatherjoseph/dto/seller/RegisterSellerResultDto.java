package ru.tinkoff.fatherjoseph.dto.seller;

import lombok.Data;

@Data
public class RegisterSellerResultDto {
    private Long sellerId;
    private String name;
    private String shopType;
    private Integer mccCode;
}
