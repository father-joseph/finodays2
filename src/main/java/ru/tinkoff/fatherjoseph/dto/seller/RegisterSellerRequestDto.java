package ru.tinkoff.fatherjoseph.dto.seller;

import lombok.Data;

@Data
public class RegisterSellerRequestDto {
    private String name;
    private String shopType;
    private Integer mccCode;
}
