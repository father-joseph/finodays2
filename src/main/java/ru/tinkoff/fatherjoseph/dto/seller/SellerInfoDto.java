package ru.tinkoff.fatherjoseph.dto.seller;

import lombok.Data;

@Data
public class SellerInfoDto {
    private String name;
    private String shopType;
    private Integer mccCode;
}
