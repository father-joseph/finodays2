package ru.tinkoff.fatherjoseph.dto.client;

import lombok.Data;

@Data
public class RegisterClientResultDto {
    private Long clientId;
    private String phone;
}
