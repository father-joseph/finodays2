package ru.tinkoff.fatherjoseph.dto.client;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RegisterClientRequestDto {
    @NotBlank
    String phone;
}
