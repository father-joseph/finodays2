package ru.tinkoff.fatherjoseph.dto.payment;

public enum PaymentResultType {
    OK, ERROR;
}
