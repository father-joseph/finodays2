package ru.tinkoff.fatherjoseph.dto.payment;

import lombok.Data;

/**
 * Checkout result for payment method. Data about payment method for user
 */
@Data
public class PaymentMethodShowDto {
    private String name;
    private String type; //  visa/sbp/mastercard
    private Integer available;
    private Integer total;
    private Integer discount; // count as checkout total - payMethod total
    private Integer cashback;
}
