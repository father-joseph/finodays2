package ru.tinkoff.fatherjoseph.dto.payment;

import lombok.Data;

/**
 * Payment method id should be specified here.
 * Details (stored in a safe place) under this id will be used to call MIR/SBP or any other payment method service API.
 */
@Data
public class PaymentMethodDetailsDto {
    private String paymentToken;
}
