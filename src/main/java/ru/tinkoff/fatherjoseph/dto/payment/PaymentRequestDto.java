package ru.tinkoff.fatherjoseph.dto.payment;

import lombok.Data;
import lombok.NonNull;

@Data
public class PaymentRequestDto {
    @NonNull
    private String basketId;
    @NonNull
    private Integer basketVersion;
    @NonNull
    private PaymentMethodDetailsDto paymentMethodDetails;
}
