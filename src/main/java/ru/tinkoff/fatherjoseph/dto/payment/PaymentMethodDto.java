package ru.tinkoff.fatherjoseph.dto.payment;

import lombok.Data;
import org.springframework.lang.Nullable;

/**
 * Nspk returned value about available payment method
 */
@Data
public class PaymentMethodDto {
    @Nullable
    private String bankId;
}
