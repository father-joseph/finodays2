package ru.tinkoff.fatherjoseph.dto.payment;

import lombok.Data;
import ru.tinkoff.fatherjoseph.dto.position.PositionDto;

import java.util.List;

@Data
public class PaymentMethodShowRequestDto {
    private List<PositionDto> positions;
    // should be more client data here
    private Long clientId;
}
