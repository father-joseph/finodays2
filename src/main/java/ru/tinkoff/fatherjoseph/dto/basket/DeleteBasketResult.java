package ru.tinkoff.fatherjoseph.dto.basket;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum DeleteBasketResult {
    OK("Ok");

    private final String value;
}
