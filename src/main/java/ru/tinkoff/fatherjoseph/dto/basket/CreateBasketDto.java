package ru.tinkoff.fatherjoseph.dto.basket;

import lombok.Data;
import lombok.NonNull;
import ru.tinkoff.fatherjoseph.dto.position.PositionDto;

import java.util.List;

@Data
public class CreateBasketDto {
    @NonNull
    private Long sellerId;

    @NonNull
    private List<PositionDto> positions;
}
