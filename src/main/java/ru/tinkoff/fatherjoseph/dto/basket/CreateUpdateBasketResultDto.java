package ru.tinkoff.fatherjoseph.dto.basket;

import lombok.Data;
import lombok.NonNull;

@Data
public class CreateUpdateBasketResultDto {
    @NonNull
    private Long basketId;
}
