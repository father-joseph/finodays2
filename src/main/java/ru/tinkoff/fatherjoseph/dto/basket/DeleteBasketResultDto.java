package ru.tinkoff.fatherjoseph.dto.basket;

import lombok.Data;
import lombok.NonNull;

@Data
public class DeleteBasketResultDto {
    @NonNull
    private DeleteBasketResult result;
}
