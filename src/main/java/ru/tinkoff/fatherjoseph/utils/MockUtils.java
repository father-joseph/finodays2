package ru.tinkoff.fatherjoseph.utils;

import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowDto;
import ru.tinkoff.fatherjoseph.dto.payment.PaymentMethodShowRequestDto;

public class MockUtils {
    /**
     * This method emulates payment system call with user basket positions
     * and returns expected price with discounts and cashback.
     */
    public static PaymentMethodShowDto mockBankService(PaymentMethodShowRequestDto request,
                                                       String name,
                                                       String type,
                                                       Integer available,
                                                       Double discountRatio,
                                                       Double cashbackRatio) {
        PaymentMethodShowDto paymentMethodShowDto = new PaymentMethodShowDto();
        paymentMethodShowDto.setName(name);
        paymentMethodShowDto.setType(type);
        paymentMethodShowDto.setAvailable(available);
        int original = request.getPositions()
            .stream()
            .map(it -> it.getPrice() * it.getQuantity())
            .reduce(Integer::sum)
            .get();
        int total = (int) Math.round(original * (1 - discountRatio));
        paymentMethodShowDto.setTotal(total);
        paymentMethodShowDto.setDiscount(original - total);
        paymentMethodShowDto.setCashback((int) (total * cashbackRatio));

        return paymentMethodShowDto;
    }
}
