drop sequence if exists basket_sequence;
drop sequence if exists position_sequence;
drop sequence if exists seller_sequence;
drop sequence if exists client_sequence;
drop table if exists positions;
drop table if exists baskets;
drop table if exists sellers;
drop table if exists clients;

create sequence basket_sequence start 1 increment 20;
create sequence position_sequence start 1 increment 20;
create sequence seller_sequence start 1 increment 20;
create sequence client_sequence start 1 increment 20;

create table clients
(
    client_id BIGINT primary key,
    phone     VARCHAR(30) not null
);

create table sellers
(
    seller_id BIGINT primary key,
    name      varchar(40) NOT NULL,
    shop_type varchar(40) NOT NULL,
    mcc_code  INTEGER     NOT NULL
);

create table baskets
(
    basket_id      BIGINT primary key,
    seller_id      BIGINT      NOT NULL,
    status         VARCHAR(30) NOT NULL,
    version        NUMERIC,
    foreign key (seller_id) references sellers (seller_id)
);

create table positions
(
    position_id BIGINT primary key,
    basket_id   BIGINT      NOT NULL,
    name        VARCHAR(50) NOT NULL,
    price       INTEGER     NOT NULL,
    quantity    INTEGER     NOT NULL,
    foreign key (basket_id) references baskets (basket_id)
);

create index on positions using btree (basket_id);
