package ru.tinkoff.fatherjoseph.utils.container;

import org.testcontainers.containers.PostgreSQLContainer;

public class JosephDbContainer extends PostgreSQLContainer<JosephDbContainer> {

    public static final JosephDbContainer INSTANCE = new JosephDbContainer();

    private JosephDbContainer() {
        super("postgres:11.11-alpine");
        withReuse(true);
        // TODO: change init.sql to liquibase migrations
        withInitScript("sql/init/joseph.sql");
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("JOSEPH_DB_URL", getJdbcUrl());
        System.setProperty("JOSEPH_DB_USERNAME", getUsername());
        System.setProperty("JOSEPH_DB_PASSWORD", getPassword());
    }
}
