package ru.tinkoff.fatherjoseph.utils.container;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.lifecycle.Startables;

public class StartContainersExtension implements Extension, BeforeAllCallback {
    @Container
    public JosephDbContainer josephDbContainer = JosephDbContainer.INSTANCE;

    @Override
    public void beforeAll(ExtensionContext context) {
        Startables.deepStart(josephDbContainer).join();
    }
}
