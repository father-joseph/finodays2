package ru.tinkoff.fatherjoseph.utils.annotation;


import org.junit.jupiter.api.extension.ExtendWith;
import ru.tinkoff.fatherjoseph.utils.container.StartContainersExtension;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ExtendWith(StartContainersExtension.class)
public @interface StartContainers {
}
