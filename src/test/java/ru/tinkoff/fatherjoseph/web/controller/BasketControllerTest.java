package ru.tinkoff.fatherjoseph.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.fatherjoseph.dto.basket.CreateBasketDto;
import ru.tinkoff.fatherjoseph.dto.position.PositionDto;
import ru.tinkoff.fatherjoseph.model.Basket;
import ru.tinkoff.fatherjoseph.model.Position;
import ru.tinkoff.fatherjoseph.repository.BasketRepository;
import ru.tinkoff.fatherjoseph.repository.PositionRepository;
import ru.tinkoff.fatherjoseph.utils.annotation.JosephTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@JosephTest
@Disabled
public class BasketControllerTest {

    private static final String URL = "/api/v1/joseph";

    public static final String CREATE_MAPPING = "/create-basket";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Test
    @Transactional // bad!
    @Sql("/sql/init/basket/create-basket-ok.sql")
    void shouldCreateBasket() throws Exception {
        List<PositionDto> positions = List.of(new PositionDto("iphone", 300, 1));
        CreateBasketDto createBasketDto = new CreateBasketDto(1L, positions);

        MvcResult mvcResult = mockMvc.perform(
                post(URL + CREATE_MAPPING)
                    .content(objectMapper.writeValueAsString(createBasketDto))
                    .contentType(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andReturn();

        Basket savedBasket = basketRepository.getReferenceById(1L);
        List<Position> savedPositions = positionRepository.findAll();

        assertEquals(savedBasket.getSellerId(), 1L);
        assertEquals(savedBasket.getVersion(), 0);
    }

}
